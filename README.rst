Contractual
============
|pypi| |bld| |cvg| |black|

Installation
--------------

.. code-block::

	pip install contractual

Use
----------

TODO fill in how to use

Contract Files
-----------------

Contract files comply with the Pact file format seen at
https://github.com/pact-foundation/pact-specification/tree/version-2
There is a key difference though-

Contractual supports mocking out arbitrary interfaces, while Pact limits itself
to http requests. As such, a Contractual file can only be used with a Pact verifier
if you limit the unchecked mocks to `HttpContractMock`. In the files themselves,
you will see a list of interactions having the form

.. code-block:: json

	{
		"description": "get all users for max",
		"request": {},
		"response": {},
		"providerState": "a user with an id named 'user' exists"
	}

That is a valid HTTP contract. A more general `ContractMock` contract would have the
form


.. code-block:: json

	{
		"description": "get all users for max",
		"contractMock": {
			"contractName": "Name",
			"args": []
		},
		"response": {},
		"providerState": "a user with an id named 'user' exists"
	}


Development
-------------

Development Environment
^^^^^^^^^^^^^^^^^^^^^^^^^

Development requirements can be installed with `poetry install`
All requirements (dev and install) are specified in pyproject.toml

Release
^^^^^^^^^^^^^

CI is handled on Gitlab at https://gitlab.com/pjbecotte/contractual. Each build
runs mypy, black, and pytest. To release a new version, a git tag is added to
master, which will result in a package being built with that version and pushed
to pypi.

.. |cvg| image:: https://gitlab.com/pjbecotte/contractual/badges/master/coverage.svg
.. |bld| image:: https://gitlab.com/pjbecotte/contractual/badges/master/pipeline.svg
.. |black| image:: https://img.shields.io/badge/code%20style-black-000000.svg
.. |pypi| image:: https://badge.fury.io/py/contractual.svg
