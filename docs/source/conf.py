import os
import re


# -- Project information -----------------------------------------------------

project = "contractual"
copyright = "2019, Paul Becotte"
author = "Paul Becotte"

release = re.sub("^v", "", os.popen("git describe --tags").read().strip())
# The short X.Y version.
version = release

extensions = ["sphinx.ext.autodoc"]
templates_path = ["_templates"]
exclude_patterns = []

pygments_style = "default"
html_theme = "sphinx_rtd_theme"
html_theme_options = {"logo_only": False}
