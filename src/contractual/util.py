class AddOnceDict(dict):
    def __init__(self, exception, **kwargs):
        self.exception = exception
        super().__init__(**kwargs)

    def __setitem__(self, key, value):
        if key in self:
            raise self.exception()
        super().__setitem__(key, value)
