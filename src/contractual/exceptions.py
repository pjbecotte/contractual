class ContractError(Exception):
    pass


class ContractStateProviderMissing(ContractError):
    pass


class ContractVerifierMissing(ContractError):
    pass


class ContractMultipleVerifiers(ContractError):
    pass


class ContractMultipleStateProviders(ContractError):
    pass


class ContractWrongSignature(ContractError):
    pass


class ContractFailedVerification(ContractError):
    pass


class ContractConflictException(Exception):
    pass
