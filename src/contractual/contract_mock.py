# pylint: disable=protected-access
from unittest.mock import MagicMock

from contractual.exceptions import ContractConflictException


class ContractCallDict(dict):
    def __setitem__(self, key, value):
        key = str(key)
        super().__setitem__(key, value)

    def __getitem__(self, item):
        return super().__getitem__(str(item))

    def __contains__(self, item):
        return super().__contains__(str(item))


class ContractMockMixin:
    __pact_key__: str = ""

    def serialize_all_calls(self):
        return [self.serialize_call(call) for call in self.mock_calls]

    def serialize_call(self, call):
        raise NotImplementedError()


class ContractMock(MagicMock, ContractMockMixin):
    __pact_key__: str = "mockCalls"

    def __init__(self, *args, **kwargs):
        provider_state = kwargs.pop("provider_state", None)
        super().__init__(*args, **kwargs)
        self._contract_calls = ContractCallDict()
        self._provider_state = provider_state

    def verify_call(self, verifier, call):
        expected = self._contract_calls[call]
        if isinstance(expected, MagicMock):
            return
        actual = verifier(*call_args(call), **call_kwargs(call))
        assert actual == expected

    def serialize_call(self, call):
        response = self._contract_calls[call]
        return {
            "description": "",
            "contractName": self._mock_name,
            # No concept of a tuple in json
            "args": list(call_args(call)),
            "kwargs": call_kwargs(call),
            "response": response,
            "providerState": self._provider_state,
        }

    def _mock_call(_mock_self, *args, **kwargs):  # pylint: disable=no-self-argument
        self = _mock_self
        try:
            ret_val = super()._mock_call(*args, **kwargs)
        except Exception as e:
            # When the side_effect raises an exception, we still want a way to record that
            # in the contract, so we store the exception type in the ret_val
            ret_val = str(type(e))
            raise
        finally:
            top_mock = _get_top_mock(mock=self)
            call_sig = top_mock.mock_calls[-1]
            if (
                call_sig in top_mock._contract_calls
                and top_mock._contract_calls[call_sig] != ret_val
            ):
                raise ContractConflictException(
                    "Tried to make two calls with the same args and different return values"
                )
            top_mock._contract_calls[call_sig] = ret_val

        return ret_val


def _get_top_mock(mock):
    top_mock = mock
    parent = mock._mock_new_parent
    seen = set()
    while parent is not None and id(parent) not in seen:
        seen.add(id(top_mock))
        top_mock = parent
        parent = top_mock._mock_new_parent

    return top_mock


# mock.call objects are tuples, and can be 2 or 3 items long, with any of
# the elements being optional. Extract the correct elements using their
# type. mock does this just using its own length, but when doing comparisons
# handles variable length objects and missing args- since I don't know where
# those could come from, we handle them as well.
def call_args(_call):
    isargs = lambda i: isinstance(i, tuple)
    return next(filter(isargs, _call), ())


def call_kwargs(_call):
    iskwargs = lambda i: isinstance(i, dict)
    return next(filter(iskwargs, _call), {})
