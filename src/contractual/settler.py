import json
from pathlib import Path
from typing import Union

from contractual.contract import Contract, ContractMeta


def reconcile(consumer_dir=None, producer_dir=None):
    consumer_contracts = None
    if consumer_dir:
        consumer_contracts = get_consumer_filenames(consumer_dir)

    providers = settle(consumer_contracts)
    if not producer_dir:
        return providers

    changed = []
    producer_dir = Path(producer_dir)
    if not producer_dir.is_dir():
        raise RuntimeError(f"producer_dir {producer_dir} is not a directory")

    for contract in providers:
        name = contract["provider"]["name"]
        path = producer_dir.joinpath(name)
        existing = {}
        if path.exists():
            with path.open("r") as f:
                existing = json.load(f)
        if existing != contract:
            changed.append(path)
            with path.open("w") as f:
                json.dump(contract, f)
    return changed


def settle(consumer_contracts=None):
    if consumer_contracts:
        import_consumer_contracts(consumer_contracts)

    provider_contracts = []
    for contract in ContractMeta.contracts.values():
        unverified = contract.verify()
        if unverified:
            provider_contracts.append(unverified)
    return provider_contracts


def get_consumer_filenames(consumer_dir: Union[str, Path]):
    consumer_dir = Path(consumer_dir)
    if not consumer_dir.is_dir():
        raise RuntimeError(f"consumer_dir {consumer_dir} is not a directory")
    return consumer_dir.glob("*")


def import_consumer_contracts(consumer_contracts):
    for file_path in consumer_contracts:
        read_input_file(file_path)


def read_input_file(file_path: Path) -> None:
    Contract.from_json_path(file_path)
