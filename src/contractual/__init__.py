try:
    from pkg_resources import DistributionNotFound, get_distribution

    __version__ = get_distribution("contractual").version
except (DistributionNotFound, ImportError):  # pragma: no cover
    # package is not installed
    __version__ = "0.0.0"
