import json
from collections import defaultdict
from contextlib import contextmanager
from functools import partial
from inspect import isgeneratorfunction
from pathlib import Path
from typing import Union
from unittest.mock import patch

import requests

from contractual.contract_mock import ContractMock
from contractual.exceptions import (
    ContractError,
    ContractFailedVerification,
    ContractMultipleStateProviders,
    ContractMultipleVerifiers,
    ContractStateProviderMissing,
    ContractVerifierMissing,
    ContractWrongSignature,
)
from contractual.http_mock import HTTMPOCK_KEY, HttpMock
from contractual.util import AddOnceDict


class MockChecker:
    def __init__(self, mock, verifier, provider):
        self.mock = mock
        self.verifier = verifier
        self.provider = provider

    def check_mock(self):
        unverified = []
        for call in self.mock.mock_calls:
            try:
                self.check_mock_call(call)
            except ContractError:
                unverified.append(self.mock.serialize_call(call))
        return unverified

    def check_mock_call(self, call):
        try:
            self.do_call(call)
        except TypeError:
            raise ContractWrongSignature()
        except AssertionError:
            raise ContractFailedVerification()

    def do_call(self, call):
        with self.provider():
            self.mock.verify_call(self.verifier, call)


class ContractMeta(type):
    contracts: dict

    def __call__(cls, provider_name=None, consumer_name=None):
        if not hasattr(ContractMeta, "contracts"):
            ContractMeta.contracts = {}
        key = (provider_name, consumer_name)
        if key not in ContractMeta.contracts:
            ContractMeta.contracts[key] = type.__call__(
                cls, provider_name, consumer_name
            )
        return ContractMeta.contracts[key]


class Contract(metaclass=ContractMeta):
    def __init__(self, provider_name=None, consumer_name=None):
        self.provider_name = provider_name
        self.consumer_name = consumer_name
        self.mocks = {}
        self.provider_states = AddOnceDict(ContractMultipleStateProviders)
        self.verifiers = AddOnceDict(ContractMultipleVerifiers)

    def mock(self, name=None, provider_state=None, **kwargs):
        mock_key = (name, provider_state)
        if mock_key not in self.mocks:
            self.mocks[mock_key] = ContractMock(
                name=name, provider_state=provider_state, **kwargs
            )
        for key, val in kwargs.items():
            setattr(self.mocks[mock_key], key, val)
        return self.mocks[mock_key]

    def http_mock(self, method, url, provider_state=None, **kwargs):
        mock_key = (HTTMPOCK_KEY, provider_state)
        if mock_key not in self.mocks:
            self.mocks[mock_key] = HttpMock(provider_state=provider_state)
        # The JSON serializer in requests-mock doesn't compact the output, so we do it
        # for them to match json returned by Flask. This may not be the correct behavior
        # for other http providers-
        if "json" in kwargs:
            kwargs["text"] = json.dumps(kwargs.pop("json"), separators=(",", ":"))
        self.mocks[mock_key].register_uri(method, url, **kwargs)
        return self.mocks[mock_key]

    def patch(self, target, create=False, **kwargs):
        mock = self.mock(target, **kwargs)
        return patch(target, new=mock, create=create)

    def patch_object(self, target, attribute, create=False, **kwargs):
        mock = self.mock(f"{type(target)}.{attribute}", **kwargs)
        return patch.object(target, attribute, new=mock, create=create)

    @contextmanager
    def patch_requests(self, method, url, provider_state=None, **kwargs):
        with self.http_mock(method, url, provider_state, **kwargs) as mocker:
            yield mocker

    def verifies(self, name):
        def decorator(func):
            self.verifiers[name] = func
            return func

        return decorator

    def provides_multiple(self, state_names):
        def decorator(func):
            func = make_context_manager(func)
            for name in state_names:
                self.provider_states[name] = func
            return func

        return decorator

    def provides(self, state_name):
        def decorator(func):
            func = make_context_manager(func)
            func = accept_arg(func)
            self.provider_states[state_name] = func
            return func

        return decorator

    def get_verifier(self, mock_name):
        try:
            return self.verifiers.get(mock_name) or self.verifiers["*"]
        except KeyError:
            raise ContractVerifierMissing()

    def get_state_provider(self, provider_state):
        if not provider_state:
            return default_given
        try:
            func = self.provider_states.get(provider_state) or self.provider_states["*"]
            return partial(func, provider_state)
        except KeyError:
            raise ContractStateProviderMissing()

    def check_mock(self, mock):
        try:
            return MockChecker(
                mock,
                self.get_verifier(mock._mock_name),  # pylint: disable=protected-access
                self.get_state_provider(
                    mock._provider_state  # pylint: disable=protected-access
                ),
            ).check_mock()
        except ContractError:
            # With no verifier or state provider, return all the calls
            return mock.serialize_all_calls()

    def verify(self):
        unverified = defaultdict(list)
        for mock in self.mocks.values():
            missed = self.check_mock(mock)
            if missed:
                unverified[mock.__pact_key__].extend(missed)
        return (
            dict(
                {
                    "metadata": {
                        "pactSpecification": {"version": "2.0.0"},
                        "pact-contractual": {"version": "1.0.0b"},
                    },
                    "provider": {"name": self.provider_name},
                    "consumer": {"name": self.consumer_name},
                },
                **unverified,
            )
            if unverified
            else None
        )

    @classmethod
    def from_json_path(cls, path: Union[str, Path]):
        path = Path(path)
        data = json.loads(path.read_text())
        provider_name = data["provider"]["name"]
        consumer_name = data["consumer"]["name"]
        contract = cls(provider_name, consumer_name)
        for mock_call in data.get("mockCalls", []):
            mock_object = contract.mock(
                mock_call["contractName"],
                provider_state=mock_call.get("providerState"),
                return_value=mock_call.get("response"),
            )
            mock_object(*mock_call.get("args", []), **mock_call.get("kwargs", {}))

        for mock_call in data.get("interactions", []):
            req = mock_call["request"]
            resp = mock_call["response"]
            mock_object = contract.http_mock(
                req["method"],
                f"mock://fakehost{req['path']}",
                mock_call["providerState"],
                text=resp["body"],
                status_code=resp["status"],
            )
            with mock_object:
                requests.request(
                    req.pop("method"), f"mock://fakehost{req.pop('path')}", **req
                )
        return contract


def make_generator(func):
    def gen(*args, **kwargs):
        yield func(*args, **kwargs)

    return gen


@contextmanager
def default_given():
    yield


def make_context_manager(func):
    if not isgeneratorfunction(func):
        func = make_generator(func)
    return contextmanager(func)


def accept_arg(func):
    return lambda _: func()
