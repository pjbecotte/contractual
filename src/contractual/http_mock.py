from typing import List, Union

import flask
import requests
import urllib3
from requests_mock import Adapter, Mocker
from requests_mock.request import _RequestObjectProxy

from contractual.contract_mock import ContractMockMixin
from contractual.exceptions import ContractConflictException

HTTMPOCK_KEY = "HTTPMOCK"


def req_key(request):
    return request.url, request.method, request.body, str(request.headers)


class MockResponse:
    """There are a number of different "Response" objects that could
    show up in a unit test. This allows us to compare across three likely
    ones (requests, urllib3, flask)
    """

    status_code: int
    headers: dict
    body: bytes

    def __init__(
        self, response: Union[requests.Response, urllib3.HTTPResponse, flask.Response]
    ):
        if isinstance(response, requests.Response):
            self.status_code = response.status_code
            self.headers = dict(response.headers)
            self.body = response.content

        if isinstance(response, urllib3.HTTPResponse):
            self.status_code = response.status
            self.headers = dict(response.headers)
            self.body = response.data

        if isinstance(response, flask.Response):
            self.status_code = response.status_code
            self.headers = dict(response.headers)
            self.body = response.data

    def matches_expected(self, expected):
        expected_headers = expected.headers.items()
        actual_headers = self.headers.items()
        return (
            self.status_code == expected.status_code
            # Flask adds an extra newline after json output- lets ignore
            # whitespace around the body
            and self.body.strip() == expected.body.strip()
            # We are only checking that specified headers exist in the actual
            # response, extra ones are ignored
            and all(item in actual_headers for item in expected_headers)
        )


class ContractAdapter(Adapter):
    def __init__(self):
        super().__init__()
        self.contract_calls = {}

    def send(self, request, **kwargs):
        resp = super().send(request, **kwargs)
        mock_resp = MockResponse(resp)
        key = req_key(request)
        if key in self.contract_calls and self.contract_calls[key] != mock_resp:
            raise ContractConflictException()
        self.contract_calls[key] = mock_resp
        return resp


class HttpMock(Mocker, ContractMockMixin):
    __pact_key__ = "interactions"

    def __init__(self, **kwargs):
        self._provider_state = kwargs.pop("provider_state", None)
        self._mock_name = HTTMPOCK_KEY
        super().__init__(adapter=ContractAdapter(), **kwargs)

    @property
    def mock_calls(self) -> List[_RequestObjectProxy]:
        return self.request_history

    def verify_call(self, verifier, call):
        expected = self._adapter.contract_calls[req_key(call)]
        actual = MockResponse(verifier(call))
        assert actual.matches_expected(expected)

    def serialize_call(self, call):
        response = self._adapter.contract_calls[req_key(call)]
        return {
            "description": "",
            "request": {
                "method": call.method,
                "path": call.path_url,
                "headers": call.headers,
            },
            "response": {
                "status": response.status_code,
                "headers": response.headers,
                "body": response.body,
            },
            "providerState": self._provider_state,
        }
