# pylint: disable=protected-access
from unittest.mock import DEFAULT, call

import pytest

from contractual.contract import Contract
from contractual.exceptions import ContractConflictException

name = "my_contract"


def test_mock_returns_value():
    m = Contract(name).mock()
    m.return_value = 99
    assert m(7) == 99


def test_returns_side_effect():
    m = Contract(name).mock()
    calls = [(5, 3), (6, 2), (7, 1)]
    m.side_effect = [x[1] for x in calls]
    for c in calls:
        assert m(c[0]) == c[1]
        assert m._contract_calls[f"call({c[0]})"] == c[1]
    with pytest.raises(StopIteration):
        m(42)


def test_side_effect_function():
    m = Contract(name).mock()
    m.return_value = 99
    m.side_effect = lambda x: 30
    assert m(1) == 30
    assert m._contract_calls[str(call(1))] == 30


def test_side_effect_exception():
    m = Contract(name).mock()
    m.return_value = 99
    m.side_effect = RuntimeError()
    with pytest.raises(RuntimeError):
        m(1)

    assert m._contract_calls["call(1)"] == str(RuntimeError)


def test_return_value_overrides_default_side_effect():
    m = Contract(name).mock()
    m.return_value = 99
    m.side_effect = lambda x: DEFAULT
    assert m(1) == 99
    assert m._contract_calls["call(1)"] == 99


def test_missing_call_not_returned():
    m = Contract(name).mock()
    with pytest.raises(KeyError):
        assert m._contract_calls["call(42)"] == 42


def test_fails_on_conflicting_responses():
    m = Contract(name).mock()
    m.return_value = 99
    m(26)
    n = Contract(name).mock()
    n.return_value = 35
    with pytest.raises(ContractConflictException):
        n(26)


def test_method_call_captured():
    m = Contract(name).mock()
    m.method.return_value = 99
    assert m.method(26) == 99
    assert m._contract_calls["call.method(26)"] == 99


def test_no_conflict_with_callargs_and_methodargs():
    m = Contract(name).mock()
    m.return_value = 3
    m.method.return_value = 99
    assert m(5) == 3
    assert m.method(26) == 99
    assert m(7) == 3
    assert m._contract_calls["call.method(26)"] == 99
    assert m._contract_calls["call(5)"] == 3
    assert m._contract_calls["call(7)"] == 3
