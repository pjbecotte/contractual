import pytest

from contractual.contract import Contract
from contractual.exceptions import ContractMultipleStateProviders

base_name = "my_contract"
func_name = "myfunc"


def test_raises_if_no_state_provider():
    contract = Contract(base_name)

    @contract.verifies(func_name)
    def _(a, b):
        return a + b

    contract.mock(func_name, provider_state="given", return_value=3)()
    unverified = contract.verify()
    assert unverified["mockCalls"][0]["contractName"] == func_name


def test_duplicate_state_provider():
    contract = Contract(base_name)

    @contract.provides("given")
    def _():
        pass

    with pytest.raises(ContractMultipleStateProviders):

        @contract.provides("given")
        def _():
            pass


def test_with_state_provider():
    contract = Contract(base_name)
    c = 99

    @contract.provides("basic")
    def _():
        nonlocal c
        c = 105

    @contract.verifies(func_name)
    def _(a):
        return a + c

    contract.mock(func_name, provider_state="basic", return_value=125)(20)
    assert not contract.verify()
    assert c == 105


def test_with_state_provider_with_cleanup():
    contract = Contract(base_name)
    c = 99

    @contract.provides("basic")
    def _():
        nonlocal c
        original = c
        c = 105
        yield
        c = original

    @contract.verifies(func_name)
    def _(a):
        return a + c

    contract.mock(func_name, provider_state="basic", return_value=125)(20)
    assert not contract.verify()
    assert c == 99


def test_multiple_state_provider():
    contract = Contract(base_name)
    c = 99

    @contract.provides_multiple(["a", "b"])
    def _(given):
        nonlocal c
        original = c
        if given == "a":
            c = 105
        else:
            c = 3
        yield
        c = original

    @contract.verifies(func_name)
    def _(a):
        return a + c

    contract.mock(func_name, provider_state="a", return_value=125)(20)
    contract.mock(func_name, provider_state="b", return_value=13)(10)
    assert not contract.verify()
    assert c == 99


def test_multiple_state_provider_no_yield():
    contract = Contract(base_name)
    c = 99

    @contract.provides_multiple(["a", "b"])
    def _(given):
        nonlocal c
        if given == "a":
            c = 105
        else:
            c = 3

    @contract.verifies(func_name)
    def _(a):
        return a + c

    contract.mock(func_name, provider_state="a", return_value=125)(20)
    contract.mock(func_name, provider_state="b", return_value=13)(10)
    assert not contract.verify()


def test_multiple_state_provider_default():
    contract = Contract(base_name)
    c = 99

    @contract.provides("a")
    def _():
        nonlocal c
        c = 10000

    @contract.provides_multiple(["*", "c"])
    def _(given):
        nonlocal c
        if given == "c":
            c = 1
        else:
            c = 0

    @contract.verifies(func_name)
    def _(a):
        return a + c

    contract.mock(func_name, provider_state="a", return_value=10020)(20)
    contract.mock(func_name, provider_state="b", return_value=10)(10)
    contract.mock(func_name, provider_state="c", return_value=12)(11)
    assert not contract.verify()
