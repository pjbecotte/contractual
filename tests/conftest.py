import pytest

from contractual.contract import ContractMeta


@pytest.fixture(autouse=True)
def contract_store():
    yield
    ContractMeta.contracts = {}
