# pylint: disable=redefined-outer-name
from pathlib import Path
from tempfile import TemporaryDirectory

import pytest
from flask import Flask

from contractual.contract import HTTMPOCK_KEY, Contract
from contractual.settler import reconcile, settle

FILE_DIR = Path(__file__).parent.joinpath("files")


@pytest.fixture
def _setup():
    contract = Contract("mycontract")
    contract.mock("contract_a", return_value=15)(10)
    contract.mock("contract_a", return_value=5)()
    contract.mock("contract_b")(10)
    contract.mock("contract_b", return_value=None)(11)

    contractb = Contract("other_provider")
    contractb.mock("contract_c", return_value=5)(10)

    @contract.verifies("*")
    def _(x=0):
        return x + 5

    contract.verifies("contract_b")(lambda x: None)
    contractb.verifies("contract_c")(lambda x: x / 2)

    return contract, contractb


def test_settle(_setup):
    assert not settle()


def test_settle_returns_missed(_setup):
    Contract("other_provider").mock("contract_z", return_value=5)(10)
    provider_contract = settle()[0]
    assert provider_contract["provider"]["name"] == "other_provider"
    assert provider_contract["mockCalls"][0] == {
        "contractName": "contract_z",
        "args": [10],
        "kwargs": {},
        "description": "",
        "response": 5,
        "providerState": None,
    }


def test_raises_validation_failure(_setup):
    Contract("other_provider").mock("contract_c", return_value=42)(11)
    assert settle()[0]["mockCalls"][0]["contractName"] == "contract_c"


def test_reads_consumer_contract(_setup):
    contract_file = FILE_DIR.joinpath("unverified.json")
    provider_contract = settle([contract_file])[0]
    assert provider_contract["provider"]["name"] == "other_provider"
    assert provider_contract["mockCalls"][0] == {
        "contractName": "contract_z",
        "args": [10],
        "kwargs": {},
        "description": "",
        "response": 5,
        "providerState": None,
    }


def test_verifies_consumer_contract(_setup):
    contract = Contract("other_provider", "test_consumer")

    @contract.verifies("contract_z")
    def _(x=0):
        return x - 5

    contract_file = FILE_DIR.joinpath("unverified.json")
    assert settle([contract_file]) == []


def test_verifies_http_contract(_setup):
    contract = Contract("other_provider", "test_consumer")

    app = Flask("test_http_verifier")
    app.route("/somepath")(lambda: "hello")

    @contract.verifies(HTTMPOCK_KEY)
    def _(req):
        client = app.test_client()
        return getattr(client, req.method.lower())(req.url)

    contract_file = FILE_DIR.joinpath("http.json")
    assert settle([contract_file]) == []


def test_fails_verification(_setup):
    contract = Contract("other_provider", "test_consumer")

    @contract.verifies("contract_z")
    def _(x=0):
        return x + 5

    contract_file = FILE_DIR.joinpath("unverified.json")
    settle([contract_file])
    assert settle([contract_file])[0]["mockCalls"][0] == {
        "contractName": "contract_z",
        "args": [10],
        "kwargs": {},
        "description": "",
        "response": 5,
        "providerState": None,
    }


def test_uses_given_state(_setup):
    contract = Contract("other_provider", "test_consumer")
    state = 0

    @contract.provides("basestate")
    def _():
        pass

    @contract.provides("extrastate")
    def _():
        nonlocal state
        state = 20

    @contract.verifies("contract_z")
    def _(x=0):
        return x - 5 + state

    contract_file = FILE_DIR.joinpath("withstate.json")
    assert settle([contract_file]) == []


def test_fails_on_missing_state(_setup):
    contract = Contract("other_provider", "test_consumer")
    state = 0

    @contract.provides("basestate")
    def _():
        pass

    @contract.verifies("contract_z")
    def _(x=0):
        return x - 5 + state

    contract_file = FILE_DIR.joinpath("withstate.json")
    assert settle([contract_file])[0]["provider"]["name"] == "other_provider"


def test_reads_consumer_contract_dir(_setup):
    unverified = reconcile(consumer_dir=FILE_DIR)
    assert len(unverified) == 1


def test_write_provider_contract():
    with TemporaryDirectory() as dirname:
        contract = Contract(provider_name="someprovider", consumer_name="me")
        contract.mock("funca", return_value=42)("arg")
        paths = reconcile(producer_dir=dirname)
        assert paths == [Path(dirname, "someprovider")]
        assert paths[0].exists()


def test_raises_on_bad_dir():
    with pytest.raises(RuntimeError):
        reconcile(producer_dir="doesntexist")
    with pytest.raises(RuntimeError):
        reconcile(consumer_dir="doesntexist")


def test_checks_provider_contract_for_changes():
    with TemporaryDirectory() as dirname:
        contract = Contract(provider_name="someprovider", consumer_name="me")
        contract.mock("funca", return_value=42)("arg")
        reconcile(producer_dir=dirname)
        paths = reconcile(producer_dir=dirname)
        assert paths == []
