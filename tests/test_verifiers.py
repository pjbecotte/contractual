import pytest
from flask import Flask, Response, jsonify
from requests import get
from urllib3 import HTTPResponse

from contractual.contract import HTTMPOCK_KEY, Contract
from contractual.exceptions import ContractMultipleVerifiers

base_name = "my_contract"
func_name = "myfunc"


def test_runs_verifier():
    contract = Contract(base_name)

    @contract.verifies(func_name)
    def _(a, b):
        return a + b

    m = contract.mock(func_name)
    m.return_value = 10
    m(6, 4)
    assert not contract.verify()


def test_verifies_kw_args():
    contract = Contract(base_name)

    @contract.verifies(func_name)
    def _(a, b=0):
        return a + b

    contract.mock(func_name, return_value=5)(5)
    contract.mock(func_name, return_value=6)(5, b=1)
    contract.mock(func_name, return_value=3)(b=1, a=2)
    assert not contract.verify()


def test_verifies_wrong_signature():
    contract = Contract(base_name)

    @contract.verifies(func_name)
    def _(a, b):
        return a + b

    contract.mock(func_name, return_value=1)()
    unverified = contract.verify()
    assert unverified["mockCalls"][0]["contractName"] == func_name


def test_verifies_wrong_signature_keywords():
    contract = Contract(base_name)

    @contract.verifies(func_name)
    def _(a, b):
        return a + b

    contract.mock(func_name, return_value=1)(c="myarg")
    unverified = contract.verify()
    assert unverified["mockCalls"][0]["contractName"] == func_name


def test_verifies_default():
    contract = Contract(base_name)

    @contract.verifies(func_name)
    def _(a, b):
        return a + b

    # Don't try and verify an unspecified return value
    contract.mock(func_name)(3, 3)
    assert not contract.verify()


def test_bad_verifier():
    contract = Contract(base_name)

    @contract.verifies(func_name)
    def _(a, b):
        return a + b

    m = contract.mock(func_name, return_value=9)
    m(6, 4)
    unverified = contract.verify()
    assert unverified["mockCalls"][0]["contractName"] == func_name


def test_cant_add_second_verifier():
    contract = Contract(base_name)

    @contract.verifies(func_name)
    def _(a, b):
        return a + b

    with pytest.raises(ContractMultipleVerifiers):

        @contract.verifies(func_name)
        def _(a, b):
            return a * b


def test_raises_if_no_verifier():
    contract = Contract(base_name)
    contract.mock(func_name)()
    unverified = contract.verify()
    assert unverified["mockCalls"][0]["contractName"] == func_name


def test_http_verifier():
    url = "http://myserver.com/mypath"
    contract = Contract()
    m = contract.http_mock("GET", url, json={"VALUE": 42})
    with m:
        response = get(url)
        assert response.json() == {"VALUE": 42}

    app = Flask("test_http_verifier")
    app.route("/mypath")(lambda: jsonify({"VALUE": 42}))

    @contract.verifies(HTTMPOCK_KEY)
    def _(req):
        client = app.test_client()
        return getattr(client, req.method.lower())(req.url)

    assert not contract.verify()


def test_urllib_response():
    url = "http://myserver.com/mypath"
    contract = Contract()
    m = contract.http_mock("GET", url, json={"VALUE": 42})
    with m:
        response = get(url)
        assert response.json() == {"VALUE": 42}

    app = Flask("test_http_verifier")
    app.route("/mypath")(lambda: jsonify({"VALUE": 42}))

    @contract.verifies(HTTMPOCK_KEY)
    def _(req):
        client = app.test_client()
        resp: Response = getattr(client, req.method.lower())(req.url)
        return HTTPResponse(
            body=resp.data,
            headers=resp.headers,
            request_url=req.url,
            status=resp.status_code,
        )

    assert not contract.verify()


def test_http_serializes_unverified():
    url = "http://myserver.com/mypath"
    contract = Contract()
    m = contract.http_mock("GET", url, json={"VALUE": 42})
    with m:
        response = get(url)
        assert response.json() == {"VALUE": 42}

    app = Flask("test_http_verifier")
    app.route("/mypath")(lambda: jsonify({"VALUE": 42}))

    assert contract.verify()["interactions"][0]["request"]["path"] == "/mypath"
