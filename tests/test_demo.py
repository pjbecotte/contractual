# from collections import defaultdict
# from unittest.mock import patch
#
#
# class MyObj:
#     def __init__(self, someval):
#         self.someval = someval
#
#     def some_func(self, arg):
#         return arg * 2
#
#
# def procedure(val):
#     o = MyObj(val)
#     return o.some_func(33)
#
#
# class ContractPatch:
#     contracts = {}
#
#     def __new__(cls, provider_name=None, consumer_name=None):
#         key = (provider_name, consumer_name)
#         if key not in cls.contracts:
#             cls.contracts[key] = cls.__init__(*key)
#         return cls.contracts[key]
#
#     def __init__(self, provider_name=None, consumer_name=None):
#         self.provider_name = provider_name
#         self.consumer_name = consumer_name
#
#
# def test_some_func():
#     with patch.object(MyObj, "some_func", return_value=5) as m:
#         assert procedure(99) == 5
#         assert len(m.mock_calls) == 1
