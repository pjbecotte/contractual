import pytest
from requests import get
from requests_mock import NoMockAddress

from contractual.contract import Contract
from contractual.exceptions import ContractConflictException


def test_http_mock_returns_value():
    url = "http://mypath.com/a"
    m = Contract().http_mock("GET", url)
    with m:
        response = get(url)
    assert response.status_code == 200


def test_raises_on_missing_path():
    url = "http://mypath.com/a"
    m = Contract().http_mock("GET", url)
    with m:
        with pytest.raises(NoMockAddress):
            get("http://other.com/b")


def test_body_response():
    url = "http://mypath.com/a"
    m = Contract().http_mock("GET", url, text="HELLO")
    with m:
        assert get(url).text == "HELLO"


def test_response_code():
    url = "http://mypath.com/a"
    m = Contract().http_mock("GET", url, status_code=500)
    with m:
        assert get(url).status_code == 500


def test_response_list():
    url = "http://mypath.com/a"
    m = Contract().http_mock(
        "GET", url, response_list=[{"status_code": 200}, {"status_code": 400}]
    )
    with m:
        assert get(url + "?a=1").status_code == 200
        assert get(url + "?a=2").status_code == 400


def test_conflicting_response():
    url = "http://mypath.com/a"
    m = Contract().http_mock(
        "GET", url, response_list=[{"status_code": 200}, {"status_code": 400}]
    )
    with m:
        assert get(url).status_code == 200
        with pytest.raises(ContractConflictException):
            get(url)
