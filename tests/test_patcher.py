# pylint: disable=too-few-public-methods,protected-access
from unittest.mock import call

import requests

from contractual.contract import Contract
from contractual.http_mock import req_key


class Example:
    def __init__(self):
        self.val = 3

    def do_something(self, arg):
        return self.val + arg


def test_patch():
    contract = Contract()
    with contract.patch("test_patcher.Example.do_something", return_value=55):
        assert Example().do_something(1) == 55

    assert Example().do_something(1) == 4
    mock = contract.mock("test_patcher.Example.do_something")
    assert mock.mock_calls[0] == call(1)
    assert mock._contract_calls[str(call(1))] == 55


def test_patch_object():
    contract = Contract()
    e = Example()
    with contract.patch_object(e, "do_something", return_value=55):
        assert e.do_something(1) == 55

    assert e.do_something(1) == 4
    mock = contract.mock(f"{type(e)}.do_something")
    assert mock.mock_calls[0] == call(1)
    assert mock._contract_calls[str(call(1))] == 55


def test_http_patch():
    contract = Contract()
    with contract.patch_requests("GET", "//mymock.fake/api/v0", text="HELLO"):
        assert requests.get("http://mymock.fake/api/v0").text == "HELLO"

    mock = contract.http_mock("GET", "//mymock.fake/api/v0")
    assert mock.mock_calls[0].url == "http://mymock.fake/api/v0"
    request = mock.mock_calls[0]
    key = req_key(request)
    assert mock._adapter.contract_calls[key].body == b"HELLO"
