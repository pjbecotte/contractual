typing:
	mypy src/contractual --ignore-missing-imports

format:
	isort -rc -tc src/contractual tests
	black src/contractual tests

lint:
	pylint src/contractual tests

unit-test:
	pytest --no-cov tests

test: format lint typing
	pytest tests

docker-test:
	docker build -t temp .
	docker run --rm temp rye
