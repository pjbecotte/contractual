FROM ubuntu:latest

WORKDIR /app

RUN apt-get update && apt-get install -y software-properties-common && rm -rf /var/lib/apt/lists/*

RUN apt-get update && apt-add-repository ppa:deadsnakes/ppa && \
  apt-get update && \
  apt-get install -y \
  	git \
    python3.6 \
    python3.7 \
    python3.8 \
    wget \
 && rm -rf /var/lib/apt/lists/*

RUN wget https://bootstrap.pypa.io/get-pip.py -O /tmp/get-pip.py \
	&& python3.7 /tmp/get-pip.py \
	&& rm /tmp/get-pip.py

RUN ln /usr/bin/python3.7 /usr/bin/python
RUN pip install --pre poetry rye

COPY pyproject.toml poetry.lock ./
RUN rye build-envs

COPY . ./
